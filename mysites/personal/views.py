from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail
# Create your views here.
from .forms import ContactForm, SignUpForm





def contact(request):
	title="Contact Us"
	form=ContactForm(request.POST or None)
	if form.is_valid():
		from_email=form.cleaned_data.get("email")
		from_message=form.cleaned_data.get("message")
		from_full_name=form.cleaned_data.get("full_name")
		subject = "email from DJANGO"
		from_email = settings.EMAIL_HOST_USER
		to_email = from_email
		contact_message = "%s : %s vai %s "%(from_full_name,from_message,from_email)
		send_mail(
			subject,
			contact_message , 
			from_email, 
			[to_email] ,
			fail_silently=True) 

	context={
		"form": form,
		"title" : title,
	}

	return render(request,"personal/forms.html",context)

	
def index(request):
	title="welcome!!"
	
	form= SignUpForm(request.POST or None)

	context={
	"title":title,
	"form" : form


	}
	
	if form.is_valid():
		#form.save()
		instance=form.save(commit=False)
		instance.save()
		context={
		"title":"Thank You sir"
		}
	return render(request,"personal/home.html",context)


